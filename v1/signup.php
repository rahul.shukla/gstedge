<?php
    include("controller/CSignup.php");
    include("model/MSignup.php");
    
    $request_method=$_SERVER["REQUEST_METHOD"];
    
    switch($request_method)
    {
        case 'POST':
            $CSignupObj = new CSignup(new MSignup());
            $CSignupObj->addUser($_REQUEST);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
?>