<?php

include("../config/connection.php");

class MLogin
{    
    public function __construct() {
        global $connection;
        $this->connection = $connection;
    }

    public function authUser($data)
    {
        $query="SELECT id,username,password,status FROM users WHERE username='".$data['username']."' AND password='".md5($data['password'])."'";
        $response=array();
        $result=mysqli_query($this->connection, $query);
        while($row=mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            $response['data']=$row;
        }
        return $response;
    }

    public function sanitize($input){
        if(get_magic_quotes_qpc($input)){

            $input = trim($input); // get rid of white space left and right
            $input = htmlentities($input); // convert symbols to html entities
            return $input;
        } else {

            $input = htmlentities($input); // convert symbols to html entities
            $input = addslashes($input); // server doesn't add slashes, so we will add them to escape ',",\,NULL
            $input = mysql_real_escape_string($input); // escapes \x00, \n, \r, \, ', " and \x1a
            return $input;
        }
    }
}

?>