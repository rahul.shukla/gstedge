<?php

include("../config/connection.php");

class MUploadExcel
{    
    public function __construct() {
        global $connection;
        $this->connection = $connection;
    }

    public function uploadFile($data)
    {
        $query="SELECT id FROM excelUpload";
        $result=mysqli_query($this->connection, $query);
        $count=mysqli_num_rows($result);
        $count++;

        $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
        $excelDir = 'media/excels/';
        if (!file_exists($excelDir)) 
        {
            mkdir($excelDir, 0777, true);
        }
        $uploadfile = $count.".".$ext;
        $uploads = move_uploaded_file($data['tmp_name'], $uploadfile);		    			
        if($uploads) 
        {
            $query = "INSERT INTO excelUpload (path)
                    VALUES ('".$uploadfile."')";
            $result=mysqli_query($this->connection, $query);
            if($result) $response['data']="File Uploded successfully.";
            else $response['message']="File upload failed.";
        }

        return $response;
    }
}

?>