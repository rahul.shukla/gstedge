<?php
    include("controller/CLogin.php");
    include("model/MLogin.php");
    
    $request_method=$_SERVER["REQUEST_METHOD"];
    
    switch($request_method)
    {
        case 'POST':
            $CLoginObj = new CLogin(new MLogin());
            $CLoginObj->checkUser($_REQUEST);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
?>