<?php

class CSignup
{
    protected $MSignupObj;
    public function __construct(MSignup $model) {
        $this->MSignupObj = $model;
    }

    public function addUser($data)
    {
        $username = $data['username'] ?? "";
        $password = $data['password'] ?? "";

        $response = array();
        $response['data']="";

        if($username == "")
        {
            $response['status'] = "0";
            $response['message'] = "Please provide username";
            header("HTTP/1.0 400 Bad Request");

        }
        else if($password == "")
        {
            $response['status'] = "0";
            $response['message'] = "Please provide password";
            header("HTTP/1.0 400 Bad Request");
        }
        else
        {
            $resData = $this->MSignupObj->addUser($data);
            
            if(!isset($resData['data']))
            {
                $response['status'] = "0";
                $response['message'] = $resData['message'];
                header("HTTP/1.0 409 Conflict");
            }
            else
            {
                $response['status'] = "1";
                $response['message'] = "";
                $response['data'] = $resData['data'];
                header("HTTP/1.0 200 OK");
            }
        }
		header('Content-Type: application/json');
		echo json_encode($response);
    }
}


?>