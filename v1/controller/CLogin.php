<?php

class CLogin
{
    protected $MLoginObj;
    public function __construct(MLogin $model) {
        $this->MLoginObj = $model;
    }

    public function checkUser($data)
    {
        $username = $data['username'] ?? "";
        $password = $data['password'] ?? "";

        $response = array();
        $response['data']="";

        if($username == "")
        {
            $response['status'] = "0";
            $response['message'] = "Please provide username";
            header("HTTP/1.0 400 Bad Request");

        }
        else if($password == "")
        {
            $response['status'] = "0";
            $response['message'] = "Please provide password";
            header("HTTP/1.0 400 Bad Request");
        }
        else
        {
            $resData = $this->MLoginObj->authUser($data);
            
            if(!isset($resData['data']))
            {
                $response['status'] = "0";
                $response['message'] = "Invalid username or password.";
                header("HTTP/1.0 401 Unauthorized");
            }
            else
            {
                $response['status'] = "1";
                $response['message'] = "";
                $response['data'] = $resData['data'];
                header("HTTP/1.0 200 OK");
            }
        }
		header('Content-Type: application/json');
		echo json_encode($response);
    }
}


?>