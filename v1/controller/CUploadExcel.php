<?php

class CUploadExcel
{
    protected $MUploadExcelObj;
    public function __construct(MUploadExcel $model) {
        $this->MUploadExcelObj = $model;
    }

    public function uploadFile($data)
    {
        $fileData = $data['excel'] ?? "";
        
        $response = array();
        $response['data']="";

        if($fileData == "")
        {
            $response['status'] = "0";
            $response['message'] = "Please upload File";
            header("HTTP/1.0 400 Bad Request");

        }
        else
        {
            $resData = $this->MUploadExcelObj->uploadFile($fileData);
            
            if(!isset($resData['data']))
            {
                $response['status'] = "0";
                $response['message'] = "File upload failed.";
                header("HTTP/1.0 401 Unauthorized");
            }
            else
            {
                $response['status'] = "1";
                $response['message'] = "";
                $response['data'] = $resData['data'];
                header("HTTP/1.0 200 OK");
            }
        }
		header('Content-Type: application/json');
		echo json_encode($response);
    }
}


?>