<?php
    include("controller/CUploadExcel.php");
    include("model/MUploadExcel.php");
    
    $request_method=$_SERVER["REQUEST_METHOD"];
    //echo '<h1>_FILES</h1>+++<pre>'; print_r($_FILES); echo '</pre>+++<br>';exit;
    switch($request_method)
    {
        case 'POST':
            $CUploadExcelObj = new CUploadExcel(new MUploadExcel());
            $CUploadExcelObj->uploadFile($_FILES);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
?>