Postman Documentation Link:

https://documenter.getpostman.com/view/4916820/Rztmqo6x


Global variables:
$connection


DB SQL file is included : 
db.sql


Config File is also included:
config/connection.php

htacces file is included:
v1/.htaccess



*********** FOLDER STRUCTURE ***********

-root
    
    -config
        -connection.php
    
    -v1
        -model
            -files
        -controller
            -files
        -route_files
        -.htaccess